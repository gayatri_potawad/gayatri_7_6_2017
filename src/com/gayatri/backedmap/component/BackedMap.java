package com.gayatri.backedmap.component;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
/**
 * this is a Map component which is wrapper of Map and has tread safe functions eg. get(), put() and containsKey()
 * @author GayatriP
 *
 */
public class BackedMap extends LinkedHashMap<Object, Object> implements Map<Object, Object> {

	private ReadWriteLock readWriteLock=null;
	private static final long serialVersionUID = 4787322296905460854L;
	
	public BackedMap(){
		readWriteLock = new ReentrantReadWriteLock();
	}
	@Override
	public void clear() {	
		super.clear();
	}

	@Override
	public boolean containsKey(Object key) {
		  Lock readLock=readWriteLock.readLock();
	        try{
	            readLock.lock();
	            return super.containsKey(key);
	        }finally{
	            readLock.unlock();
	        }
	}

	@Override
	public boolean containsValue(Object value) {
		return super.containsValue(value);
	}

	@Override
	public Set<java.util.Map.Entry<Object, Object>> entrySet() {
		return super.entrySet();
	}

	@Override
	public Object get(Object key) {
		 Lock readLock=readWriteLock.readLock();
	     try{
	       readLock.lock();
	       return super.get(key);
	     }finally{
	        readLock.unlock();
	     }
	}

	@Override
	public boolean isEmpty() {
		return super.isEmpty();
	}

	@Override
	public Set<Object> keySet() {
		return super.keySet();
	}

	@Override
	public Object put(Object key, Object value) {
	     return super.put(key, value);
	}

	@Override
	public void putAll(Map<? extends Object, ? extends Object> m) {
		for (java.util.Map.Entry<? extends Object, ? extends Object> entry : m.entrySet()){
			put(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public Object remove(Object key) {
		Lock writeLock=readWriteLock.writeLock();
        try{
            writeLock.lock();
            return super.remove(key);
        }finally{
            writeLock.unlock();
        }
	}

	@Override
	public int size() {
		return super.size();
	}

	@Override
	public Collection<Object> values() {
		return super.values();
	}

}
