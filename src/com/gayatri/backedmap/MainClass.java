package com.gayatri.backedmap;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import com.gayatri.backedmap.bean.UserProfile;
import com.gayatri.backedmap.component.Constants;
import com.gayatri.backedmap.service.BackedHashMap;
import com.gayatri.backedmap.service.MapService;

public class MainClass {

	public static void main(String[] args) {
		BackedHashMap backedHashMap = new BackedHashMap();
		int choice = 0;
		BufferedReader br = null;
		boolean appRunning = true;
		try{
			 br = new BufferedReader(new InputStreamReader(System.in));
			 while(appRunning){
					System.out.println("___________________________________________________");
					System.out.println(" 1. add element to a map \n 2. search element by ID \n 3. display cached values \n 4. exit \n 5. set cache max size");
					System.out.println("__________ enter choice code from above ___________");
					try{
						choice = Integer.parseInt(br.readLine());
					}catch (Exception e) {
						choice = 0;
					}
					switch (choice) {
					case 1:			
						System.out.println("enter id name and addr of the user");
						try{
							int id = Integer.parseInt(br.readLine());
							String userName = br.readLine();
							String address = br.readLine();
							boolean added= backedHashMap.put(id, new UserProfile(id, userName, address));
							System.out.println("added= "+added);
						}catch (Exception e) {
							System.out.println("error occurerd Enter data carefully");
						}
						break;
					case 2:		
						System.out.println("enter id to be searched");
						try{
							int key = Integer.parseInt(br.readLine());
							System.out.println(backedHashMap.get(key));
						}catch (Exception e) {
							System.out.println("error occurerd Enter data carefully");
						}
						break;
					case 3:			
						System.out.println("current cached data is:");
						System.out.println(backedHashMap.getCachedData());
						break;
					case 4:			
						appRunning = false;
						MapService service = backedHashMap.getmMapService();
						service.backupCache();
						break;
					case 5:		
							System.out.println("The map limit is 5 elements and this Enter limit for current session");
						try{
							int maxSize = Integer.parseInt(br.readLine());
							Constants.CACHE_MAP_SIZE = maxSize;
						}catch (Exception e) {
							System.out.println("error occurerd Enter data carefully");
						}
						break;
					default:
						System.out.println("wrong choice !!");
						break;
					}
				}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}
}
