package com.gayatri.backedmap.tasks;

import java.util.concurrent.Callable;

import com.gayatri.backedmap.component.Constants;
import com.gayatri.backedmap.service.MapService;

public class MapGetCallable implements Callable<Object>{

private long waitTime;
Object key;
int lookupCache = 0;
private MapService mMapService = null;
	public MapGetCallable(int timeInMillis,MapService mMapService,Object key,int lookupCache){
		this.waitTime=timeInMillis;
		this.key =  key;
		this.mMapService = mMapService;
		this.lookupCache = lookupCache;
	}
	
	@Override
	public Object call() throws Exception {
		if(lookupCache == Constants.CACHE_LOOKUP_YES){
			return mMapService.getmCacheMap().get(key);
		}else if(lookupCache == Constants.CACHE_LOOKUP_NO){
			return mMapService.searchMissedKey(key);
		}else{
			return null;
		}
	}

}
