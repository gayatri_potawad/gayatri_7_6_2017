package com.gayatri.backedmap.tasks;

import java.util.concurrent.Callable;

import com.gayatri.backedmap.service.MapService;

public class FileWriteCallable implements Callable<Object>{

private long waitTime;
private MapService mMapService = null;
	public FileWriteCallable(int timeInMillis,MapService mMapService){
		this.waitTime=timeInMillis;
		this.mMapService = mMapService;
	}
	@Override
	public Object call() throws Exception {
		mMapService.backupCache();
		return null;
	}

}
