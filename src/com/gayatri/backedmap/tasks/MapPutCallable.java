package com.gayatri.backedmap.tasks;

import java.util.concurrent.Callable;

import com.gayatri.backedmap.service.MapService;

public class MapPutCallable implements Callable<Object>{

private long waitTime;
Object key;
Object value;
private MapService mMapService = null;
	public MapPutCallable(int timeInMillis,MapService mMapService,Object key,Object value){
		this.waitTime=timeInMillis;
		this.key =  key;
		this.value = value;
		this.mMapService = mMapService;
	}
	@Override
	public Object call() throws Exception {
		return mMapService.getmCacheMap().put(key, value);
	}

}
