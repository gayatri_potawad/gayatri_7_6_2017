package com.gayatri.backedmap.bean;

import java.io.Serializable;
/**
 * this class wraps a data to be flushed to mapDB for any object
 * @author GayatriP
 *
 */
public class UserProfile implements Serializable {
	private int id;
	private String userName;
	private String address;
	
	public UserProfile(int id, String userName,String address){
		this.id = id;
		this.userName = userName;
		this.address = address;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Override
	public String toString() {
		return "UserProfile[id="+id+", userName="+userName+", address="+address+"]";
	}
}
