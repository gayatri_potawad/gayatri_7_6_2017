package com.gayatri.backedmap.bean;
/**
 * this class wraps the value of file related information which will be mapped to its index
 * @author GayatriP
 *
 */
public class FileIndex {
	private String fileName;

	public FileIndex(String fileName){
		this.fileName = fileName;
	}
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	@Override
	public String toString() {
		return "FileIndex[fileName="+fileName+"]";
	}
}
