package com.gayatri.backedmap.service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import com.gayatri.backedmap.component.BackedMap;
import com.gayatri.backedmap.component.Constants;
import com.gayatri.backedmap.tasks.FileWriteCallable;
import com.gayatri.backedmap.tasks.MapGetCallable;
import com.gayatri.backedmap.tasks.MapPutCallable;
/**
 * this is a service provide or controller class which creates thread for requests and return service from MapService
 * this class has get() and put() operations
 * @author GayatriP
 *
 */
public class BackedHashMap{
	private MapService mMapService = null;
	public BackedHashMap(){
		mMapService = new MapService();
		mMapService.initMapService();
	}
	
	/**
	 * this method checks if map limit is touched if yes then dump map to a file
	 * and clears map to store requested object
	 * if no then continue and store requested object to map (cache)
	 * @param key
	 * @param value
	 * @return boolean
	 */
	public boolean put(Object key, Object value){
		Object response = null;
		MapPutCallable mapPutCallable = null; 
		FileWriteCallable fileWriteCallable = null;
		FutureTask<Object> asyncTask = null;
		ExecutorService executor = null;
		if(mMapService.getmCacheMap() != null){
			if(mMapService.getmCacheMap().size() >= Constants.CACHE_MAP_SIZE){
				fileWriteCallable = new FileWriteCallable(1000, mMapService);
				asyncTask = new FutureTask<>(fileWriteCallable);
				executor = Executors.newFixedThreadPool(2);
				executor.execute(asyncTask);
				while (true) {
					try {
						if(asyncTask.isDone()){
							executor.shutdown();
							break;
						}
						response= asyncTask.get();
					} catch (InterruptedException | ExecutionException e) {
						e.printStackTrace();
					}
				}
			}
			mapPutCallable = new MapPutCallable(1000, mMapService, key, value);
			asyncTask = new FutureTask<>(mapPutCallable);
			executor = Executors.newFixedThreadPool(2);
			executor.execute(asyncTask);
			while (true) {
				try {
					if(asyncTask.isDone()){
						executor.shutdown();
						break;
					}
					response= asyncTask.get();
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
			}
		}				
		return true;
	}
	
	/**
	 * this method checks if cache or map has requested object
	 * if data miss then picks it from file and return object
	 * @param key
	 * @return Object
	 */
	public Object get(Object key){
		Object response = null;
		MapGetCallable mapGetCallable = null;
		FutureTask<Object> asyncTask = null;
		ExecutorService executor = null;
		if(mMapService.getmCacheMap() != null){
			mapGetCallable = new MapGetCallable(1000,mMapService,key,Constants.CACHE_LOOKUP_YES);
			asyncTask = new FutureTask<Object>(mapGetCallable);
			executor = Executors.newFixedThreadPool(2);
			executor.execute(asyncTask);
			while (true) {
				try {
					if(asyncTask.isDone()){
						executor.shutdown();
						break;
					}
					response= asyncTask.get();
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
			}
			if(response == null){
				mapGetCallable = new MapGetCallable(1000,mMapService,key,Constants.CACHE_LOOKUP_NO);
				asyncTask = new FutureTask<Object>(mapGetCallable);
				executor = Executors.newFixedThreadPool(2);
				executor.execute(asyncTask);
				while (true) {
					try {
						if(asyncTask.isDone()){
							executor.shutdown();
							break;
						}
						response= asyncTask.get();
					} catch (InterruptedException | ExecutionException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return response;	
	}
		       
	public BackedMap getCachedData(){
		BackedMap response = null;
		if(mMapService.getmCacheMap() != null){
			response = mMapService.getmCacheMap();
		}
		return response;	
	}

	public MapService getmMapService() {
		return mMapService;
	}

	public void setmMapService(MapService mMapService) {
		this.mMapService = mMapService;
	}	
}
