package com.gayatri.backedmap.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import com.gayatri.backedmap.component.BackedMap;
import com.gayatri.backedmap.component.Constants;

/**
 * this is a service class which allows map and file functionalities
 * @author GayatriP
 *
 */
public class MapService {
	FileOutputStream fos = null;
	 ObjectOutputStream oos = null;
	 private BackedMap mMapIndex = null;
	 private BackedMap mCacheMap = null;
	 BackedMap mTempBackedMap = null;
	 private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
	public void initMapService(){
		mMapIndex = readFromFile("indexStore.ser");
		mCacheMap = readFromFile("cachemap.ser");
		if(mMapIndex == null){
			mMapIndex = new BackedMap();
		}
		if(mCacheMap == null){
			mCacheMap = new BackedMap();
		}
	}
	public void flushToFile(BackedMap backedMap,String fileName){
		  Lock writeLock=readWriteLock.writeLock();
		 try
         {
			 writeLock.lock();
                fos =  new FileOutputStream(fileName);
                oos = new ObjectOutputStream(fos);
                oos.writeObject(backedMap);
                oos.close();
                fos.close();
         }
		 catch(IOException ioe){
               // ioe.printStackTrace();
         }
		 catch(Exception ioe) {
              ioe.printStackTrace();
         }
 		 finally{
 			writeLock.unlock();
 	     }
	}
	
	public BackedMap readFromFile(String fileName){
		Lock readLock=readWriteLock.readLock();
		BackedMap psCacheMap = null;
		 try
         {
			 readLock.lock();
			 FileInputStream fis = new FileInputStream(fileName);
		     ObjectInputStream ois = new ObjectInputStream(fis);
		     psCacheMap = (BackedMap) ois.readObject();
		     ois.close();
         }
		 catch(IOException ioe){
			 //ioe.printStackTrace();        
		 }
		 catch(Exception ioe) {
             ioe.printStackTrace();
         }
		 finally{
	         readLock.unlock();
	     }
		return psCacheMap;
	}
	public BackedMap getmMapIndex() {
		return mMapIndex;
	}
	public void setmMapIndex(BackedMap mMapIndex) {
		this.mMapIndex = mMapIndex;
	}
	public BackedMap getmCacheMap() {
		return mCacheMap;
	}
	public void setmCacheMap(BackedMap mCacheMap) {
		this.mCacheMap = mCacheMap;
	}
	
	public void backupCache(){
		flushToFile(mCacheMap, "cachemap.ser");
		if(mCacheMap != null){
			for (java.util.Map.Entry<? extends Object, ? extends Object> entry : mCacheMap.entrySet()){
				if(Integer.parseInt(entry.getKey().toString()) > 0){
					int key = Integer.parseInt(entry.getKey().toString());
					int hashKey = key/Constants.CACHE_MAP_SIZE;
					String fileName = null;
					if(mMapIndex.containsKey(hashKey)){
						fileName = (String) mMapIndex.get(hashKey);
						mTempBackedMap = readFromFile(fileName);
					}
					else{
						fileName = hashKey+".ser";
						mMapIndex.put(hashKey,fileName);
					}
					
					if(mTempBackedMap == null){
						mTempBackedMap = new BackedMap();
					}
					mTempBackedMap.put(entry.getKey(), entry.getValue());
					flushToFile(mTempBackedMap, fileName);				
				}
			}
			mCacheMap.clear();
			mTempBackedMap.clear();
		}
		
		backupIndexEntries();
	}
	public void backupIndexEntries(){
		flushToFile(mMapIndex, "indexStore.ser");
	}
	
	public BackedMap findMissedEntry(Object key){
		int hashKey = (Integer.parseInt(key.toString()))/Constants.CACHE_MAP_SIZE;
		String fileName =  null;
		mTempBackedMap = null;
		if(mMapIndex.containsKey(hashKey)){
			fileName = (String) mMapIndex.get(hashKey);			
		}
		else{
			fileName = hashKey+".ser";
			mMapIndex.put(hashKey,fileName);
		}
		mTempBackedMap = readFromFile(fileName);		
		return mTempBackedMap;
	}
	
	public Object searchMissedKey(Object key){
		mTempBackedMap = findMissedEntry(key);
		if(mTempBackedMap != null){
			if(mTempBackedMap.containsKey(key)){
				Object object = mTempBackedMap.get(key);
				if(object!=null){
					mCacheMap.put(key, object);
					return object;
				}
			}
		}
		return null;		
	}
}
